FROM python:3.9

ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES compute,utility

RUN pip3 install --upgrade pip && pip install --upgrade pip && pip3 install torch numpy torchvision matplotlib networkx zmq jsonlines pillow smallworld localconfig PyWavelets pandas crudini scikit-learn lz4 fpzip && apt update -y && apt upgrade -y && apt install -y dnsutils net-tools

COPY ./scripts /scripts

COPY ./decentralizepy /decentralizepy
WORKDIR decentralizepy
RUN pip3 install --upgrade pip && pip install --upgrade pip && pip3 install --editable .[dev]
WORKDIR /
ENTRYPOINT ["/bin/bash", "/scripts/client.sh"] 
