.. image:: https://upload.wikimedia.org/wikipedia/commons/f/f4/Logo_EPFL.svg
   :alt: EPFL logo
   :width: 75px
   :align: right

==============
decentra-learn
==============

Repository incorporating decentralizepy and kollaps to emulate realistic networks.
Code for studying server unavailabilities.

-------------------------
Setting up decentra-learn
-------------------------

* Run ``git clone --branch main --depth 1 --recurse-submodules git@gitlab.epfl.ch:randl/decentra-learn.git``
* Then, run ``cd decentra-learn/decentralizepy && git fetch && git checkout timeout && cd ..``
* You are almost ready to go! Follow the instructions in tuto.md in order to run your first experiment.

------
Citing
------

Cite us as ::

    @article{dhasade2023decentralized,
    title={Decentralized Learning Made Easy with DecentralizePy},
    author={Dhasade, Akash and Kermarrec, Anne-Marie and Pires, Rafael and Sharma, Rishi and Vujasinovic, Milos},
    journal={arXiv preprint arXiv:2304.08322},
    year={2023}
    }

------------
Contributing
------------

* ``isort`` and ``black`` are installed along with the package for code linting.
* While in the root directory of the repository, before committing the changes, please run ::

    black .
    isort .
