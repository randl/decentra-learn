# A tutorial on how to use Kollaps with Decentralizepy

First, make sure to follow the instructions at README.rst
## What Kollaps does
Kollaps can : 
- Run every docker image you want in parallel
- Control the quality of the network that these images can use to communicate with each other, by controlling the quality of links (throughput, drop rate, delay...).
- (I don't use that) Change the characteristics of the network and the liveness of the images dynamically. For example, we can specify that an image should crash artifically at T=120s, or that a link should half its drop rate at T=20s.

This enables to run simulations of the docker images on a bad network, for example mimicking the Internet while actually running on a cluster with excellent network characteristics.
Kollaps can also distribute this work on several interconnected machines, in order to run a large amount of containers. This happens transparently in the sense that the Docker images are not aware of this.

## Installing and setting up Kollaps
Do yourself a favor and add your own user to the 'docker' group, enabling you to run all docker commands without sudo : 
```
sudo usermod -aG docker $USER
```
### Docker install (run once on every node)
Kollaps requires Docker to run, for two reasons. First, the software that the user (you) is running will be executed within a Docker container in order to behave like a full computer dedicated to this instance of the software. Second, the Kollaps controller that slows down messages at runtime will itself be executed within a container on the same machine. 
Follow the instructions to install Docker [here](https://docs.docker.com/engine/install/ubuntu). Note that this step was already executed on sacs002, sacs004 and sacs005. No need to repeat this more than once.

### Docker setup (run once on every node)
We will use a feature of Docker called Docker Swarm, which groups several machines and automates the distribution of work across these machines.
- Run ```docker swarm init``` on ONE machine. This machine is now part of a swarm of size 1.
- (optional) If you want to work with more machines than that:
    + run ```docker swarm join-token manager``` on the same ONE machine to get the password that enables other machines to join the swarm. Note that Kollaps requires all members of the swarm to be 'managers' (i.e. admins), which is why this command ends with ```manager``` and not with ```worker```. 
    + copy paste the command this outputs into every other machine that should be a member of the swarm. 

We now have a functional Docker Swarm. We can then prepare the network that the images will use to communicate. On any node, but only once in the swarm, run ```docker network create --driver=overlay --subnet=10.1.0.0/24 kollaps_network```. This means that every docker container that runs on the swarm will receive an IP address in the form 10.1.0.X where 1 <= X <= 254. Note that this means that simulations will be limited to 254 containers running at once. If that's an issue, see the Kollaps README for an alternative address space.

### Kollaps inital setup and examples (run once on every node)

![The Kollaps building pipeline](kollaps_pipeline.png "interesting image title")

Then, we will get an example from the Kollaps collection to compile and run on this setup.
- Run the following on every node in the Swarm: 
```bash
cd Kollaps
export DOCKER_BUILDKIT=1
docker build --rm -f dockerfiles/Kollaps -t kollaps:2.0 .
docker build -f dockerfiles/DeploymentGenerator -t kollaps-deployment-generator:2.0 .
```
Which will create Docker images named `kollaps` and `kollaps-deployment-generator` that are used internally by Docker. We will not control these by hand. 

Then, we can build an actual toy expriment to check that everything works well:
```
cd examples
./KollapsDeploymentGenerator ./iperf3/topology.xml -s topology.yaml
./KollapsAppBuilder iperf3 
```
The last two commands are interesting. 

The first one transforms a topology description (`./iperf3/topology.xml`) into a docker Swarm (`-s`) file named `topology.yaml`. The description contains the list of containers to run, the network links that connect them and optionally some dynamic events, like crashes and updates to network characteristics. The docker swarm file that it outputs can be run on the swarm directly to run the experiment described in the topology.

The second command generates a docker container from the name of an example from Kollaps. We cannot reuse this one for other blackboxes like Decentralizepy, because it is hardcoded for the Kollaps examples.

By now, running `docker images` should give somthing along the lines of 
```
REPOSITORY                     TAG       IMAGE ID       CREATED         SIZE
kollaps/dashboard              1.0       e78cce08bc5f   5 minutes ago   350MB
kollaps/iperf3-client          1.0       6ce3f6bab3ac   5 minutes ago   17MB
kollaps/iperf3-server          1.0       32031361300c   5 minutes ago   17MB
kollaps-deployment-generator   2.0       d3881a55f7a0   5 minutes ago   617MB
kollaps                        2.0       c68fbc5b182c   6 minutes ago   641MB
```
and now, we can start the experiment : 

`docker stack deploy -c topology.yaml kollaps_example`

Assuming all worked well, you can reach the dashboard website at `http://<your ip>:8088`. Click start to start the experiment, and stop when you want.

Known issue on multi-node swarm clusters : the dashboard spaws on a random node of the swarm. Try other IPs in the swarm if it doesn't work directly. Do not trust `docker ps` to tell you where the dashboard cluster is running, it's lying. (it's not lying, but docker redirects http connections however it wants. haven't found a way to control it. See 'docker swarm mode routing mesh' for more info).

In order to see the logs of the experiments, you can use `docker exec -it <id> bash` with the container id that you can obtain from `docker ps`. Logs are located in /var/log. Use Ctrl+D to leave the container. To stop the experiment, first click on stop in the dashboard, and then `docker stack rm kollaps_example`. The experiment is over, congrats !

## Our own example
- We will run our own example locally. For this, we provide a Dockerfile at the root of the project that runs decentralizepy over Kollaps.
- First of all, we will `cd` back to the root of the repo and run `docker build -t kollaps/dp:1.0 .` to create the Docker image that contains Decentralizepy along with a command to run on startup.
- Second, we will build a network from the topology file using the command `Kollaps/examples/KollapsDeploymentGenerator topologies/topology.xml -s topologies/topology.yaml` : this runs the topology compiler (KollapsDeploymentGenerator) on the input file `topology/topology.xml` and outputs `topology/topology.yaml`. You may be asked to confirm the deletion of the previous build, which you should accept. 
Note that you can update the contents of the XML file as you wish to best fit your scenario. 
- Finally, you can deploy your experiment with `docker stack deploy -c topologies/topology.yaml dp`. Update the topology name according to what you specified as the output of the executable in the previous command. The last word `dp` refers to the name of this experiment according to Docker. Make sure no two experiments are running concurrently with the same name (Docker will complain).
- On your browser, you can now access the dashboard at the same address as the iperf tutorial before, i.e. at `http://<your ip>:8088`. Launch the experiment by clicking on Start. Stop your experiment by clicking on Stop. In the meantime, you can run `docker ps` to list your running containers and `docker exec -it <container id> bash` to run commands in one of them.
- Once your experimented is stopped through the dashboard, you can run `docker stack rm dp` to ask Docker to terminate all containers related to the experiment.

If you wish to change the code the containers are running, know that they always execute what is contained in `scripts/client.sh`. Change this code to update the behavior of the containers at will.

## Known first issues
If the dashboard does not spawn in a multi-host swarm environment, the Kollaps boot sequence had an issue. Most likely, Kollaps tried to boot but did not find the topology.xml/yaml files on all machines, yet they are required on each of them for the simulation of the network. As far as I know, all copies of the topology files must have the same path.
`docker service ps kollaps_example_bootstrapper --no-trunc` and `docker stack ps dp --no-trunc` (replace dp with your experiment name if you changed it) are your friends for further diagnosis of the Kollaps boot steps.



## Cleanup
If you want to remove eveything related to Docker containers, run `docker system prune -a`. This will remove all images, networks and build cache that are not currently used from Docker. Note that this will wipe all containers, including the ones that are not related to this repository. 
To be more precise in your deletions, run `docker images` for a list of images installed, and then `docker rmi <list of ids>` will remove the images you want gone. 
