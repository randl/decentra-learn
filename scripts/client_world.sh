
host milan-$KOLLAPS_UUID | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sort -u | tee ips.txt
host saopaulo-$KOLLAPS_UUID | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sort -u | tee -a ips.txt
host capetown-$KOLLAPS_UUID | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sort -u | tee -a ips.txt
host tokyo-$KOLLAPS_UUID | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sort -u | tee -a ips.txt
host stockholm-$KOLLAPS_UUID | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sort -u | tee -a ips.txt


selfip=$(ifconfig eth0 | awk '/inet / {print $2}')
echo $selfip | tee /selfip.txt
python3 /scripts/pre_runner.py
cd decentralizepy/tutorial
./remote_run_timeout.sh
