host worker-$KOLLAPS_UUID | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sort -u | tee ips.txt
selfip=$(ifconfig eth0 | awk '/inet / {print $2}')
echo $selfip | tee /selfip.txt
python3 /scripts/pre_runner.py
cd decentralizepy/tutorial
./remote_run_timeout.sh
cd /decentralizepy/eval/2*
cp -r machine* /docker_logs/ 